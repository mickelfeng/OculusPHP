# OculusPHP
This PHP class features an easy to use API which allows easy access to external content using HTML tagname and / or attributes.
Crawling external pages with this PHP class can easily demand lots of ressources from the server.
If you're using this class, try caching the results.

## Use the Crawler
### Setup
Call an instance of the crawler class and set some options.
```php
$crawler = new Oculus();
$crawler->set_page('http://www.google.de/');
$crawler->set_excludes(['script', 'style', 'meta', 'noscript', 'head']);
```
If you're ready, start the initial crawl by calling the `crawl()` method.
```php
$crawler->crawl();
```
### Query content
Now, you can get query content by using an attribute selector or the tagname:
```php
// attribute selector
$query = $crawler->get('attr', 'logo', 'id');

// tagname selector
$query = $crawler->get('tag', 'div');
```
If you want to chain a search, you can pass the first result to the next query.
This allows searching only within the search results.
```php
// This will get  all .content in #header
$query = $crawler->get('attr', 'header', 'id');
$query = $crawler->get('attr', 'content', 'class', $query);
```
If you want to combine multiple search results into one, you can pass the old results to the following search query.
```php
// This will get both the main and footer navigation
$query = $crawler->get('attr', 'navigation-main', 'id');
$query = $crawler->get('attr', 'navigation-footer', 'id', false, $query);
```
If your querys get too many results, you can reduce them to a set max value
```php
$var = 10; // only 10 results
$query = $crawler->limit($query, $var);
```
### Follow Links
You can follow the **first link** in the current query results by calling `follow()`. This will automaticly update the crawler url and kick off a new `crawl()`. This means, you can continue to query for results like you did before, just on the new page.
```php
// Old page
$query = $crawler->get('tag', 'a');
$crawler->follow($query);

// New page
$query = $crawler->get('tag', 'h1');
```
If you wish to follow all links in the current query results, call `follow_each()`. You have to set a manual callback, to handle the crawls.
```php
// The custom callback
function callback($crawler, $url){

	echo 'Currentling crawling: ' . $url . '<br />';
	$query = $crawler->get('tag', 'h1');
	
	// you need to return the query, so the follow_each() function
	// can return an array with the combined results of all pages
	return $query;
}

$query = $crawler->get('tag', 'a', '', $query);
$query = $crawler->follow_each($query, 'callback');
```
### Output
If you want to further use the crawled content, you could just use the query results, but these carry a lot of garbage with them, you dont need anymore. To finalize your query:
```php
$query = $crawler->get('tag', 'a');
$query = $crawler->value($query);
```
## Use the Cronjob API
If you need to crawl multiple external Pages, it is highly recommendet, to kick of the Crawling by using the OculusCron Cronjob API.
### Setup
In the cronjob accessed file, get an Instance of the `CronOculus`-Class. This class extends the basic `Oculus`-Class to allow Cronjob functionality. For a evenly formatted output you need to set an Output Format. This will come into play later.
```php
$crawler = new CronOculus();
$crawler->set_output_format(['headline', 'imgs']);
```
### Add URLs
You can add URLs by using the `add_url()` method. Because every external Page is different, you need to pass a custom callback with each URL. Of course you can use the same callback for multiple Pages.
```php
function google_callback($crawler, $url){
	// $crawler 	the current crawler instance
	// 				this will give you access to all Oculus querys

	// $url 		the currently crawled URL
}
$crawler->add_url('http://www.google.com/', 'google_callback');
```
Within the custom callback, you can use all basic querys and follows like, when you're using `Oculus` normally.
### The custom Callback
Because you may crawl a lot of different pages, you need to use the early defined global output format. After you've filled the `$format` object, you need to return it *(or multiple output format objects)* in a single array.
```php
function google_callback($crawler, $url){

	// Get the output format
	$format = $crawler->get_output_format();

	/**
	 * Fill up $format here
	 */

	$return = [];
	array_push($return, $format);

	return $return;
}
```
### Follow within the custom Callback
If you follow multiple links within a single crawled URL by using the `follow_each()` method. You can use the same custom callback as you did for the root url of the currently crawled homepage. You can differentiate the passes by using the `is_root_url()` method.
```php
function google_callback($crawler, $url){
	$format = $crawler->get_output_format();
	if ($crawler->is_root_url($url)) {
		// InitialcCrawl which follows all links
		$query = $crawler->get('tag', 'a');

		// Will return array of all followed results
		$query = $crawler->follow_each($query, 'google_callback');
		return $query;
	}
	else{
		// Followed crawlings
		$headline = $crawler->get('tag', 'h1');
		$headline = $crawler->value($headline);

		// The output format instance is filled here
		$format->headline = $headline;

		// Return the filled output format
		return $format;
	}
}
```
### Start the Cronjob Crawling
After you've set up all URLs and Callbacks, you can kick of the Crawler by using
```php
$crawler->start();
```
## ToDO
- add set functions to the cron directorys
- finish missing docs here